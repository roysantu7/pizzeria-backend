const express = require('express')
const cors = require('cors')
const mongodb = require('mongodb')
const mongoClient = mongodb.MongoClient
const url = 'mongodb://localhost:27017'
const dbName = 'pizzeria'
const app = express()
const port = 3000

mongoClient.connect(url, function(err, client){
    if(err){
        console.log(err)
    }
    else{
        db = client.db(dbName);
        collection = db.collection('products');
        cartCollection = db.collection('cart');
        ingCollection = db.collection('ingredients');
        customCartCollection = db.collection('customcart');
    }
})

app.use(cors());
app.use(express.json());

app.get('/getAllProducts', (req, res) => {
    collection.find().toArray(function(err,doc){
        if(err){
            console.log(err)
        }
        else{
            res.json(doc)
        }
    })
})

app.get('/getAllFromCart', (req, res) => {

    cartCollection.aggregate([
        { $lookup:
           {
             from: 'products',
             localField: 'id',
             foreignField: 'id',
             as: 'cartdetails'
           }
        }
        ]).toArray(function(err, result) {
        if (err) throw err;
        res.json(result)
      });

})

app.post('/addOrder', function(req, res) {
    cartCollection.insertOne(req.body, function(err, result){
        if(err){
            console.log(err)
        }
        else{
            res.json({msg:"data inserted"});
        }
    })
});

app.post('/addCustomOrder', function(req, res) {
    customCartCollection.find({id:req.body.id}).toArray(function(err,doc){
        if(err){
            console.log(err)
        }
        else {
            if(doc.length < 1){
                customCartCollection.insertOne(req.body, function(err, result){
                    if(err){
                        console.log(err)
                    }
                    else{
                        res.json({msg:"data inserted"});
                    }
                })
            }
            else{
                customCartCollection.remove({id:req.body.id})
                res.json({msg:"data removed"});
            }
        }
    })
    
});


app.get('/getAllIngredients', (req, res) => {
    ingCollection.aggregate([
        { $lookup:
           {
             from: 'customcart',
             localField: 'id',
             foreignField: 'id',
             as: 'cartdetails'
           }
        }
        ]).toArray(function(err, result) {
        if (err) throw err;
        res.json(result)
      });
});

app.post('/removeFromCart', function(req, res) {
    cartCollection.deleteOne({id:req.body.id}, function(err, result){
        if(err){
            console.log(err)
        }
        else{
            console.log("Data deleted from cart");
            res.json({msg:"data inserted"});
        }
    })
});

app.get('/removeFromCustomCart', function(req, res) {
    console.log("inside delete")
    customCartCollection.remove({}, function(err, result){
        if(err){
            console.log(err)
        }
        else{
            console.log("Data deleted from custom cart");
            res.json({msg:"data deleted"});
        }
    })
});

app.get('/getCustomCart', (req, res) => {
    console.log("inside node get");

    customCartCollection.aggregate([
        { $lookup:
           {
             from: 'ingredients',
             localField: 'id',
             foreignField: 'id',
             as: 'cartdetails'
           }
        }
        ]).toArray(function(err, result) {
        if (err) throw err;
        res.json(result)
      });

});

app.listen(port, () => console.log(`Connected...`))